texture_player = Texture.new( "images/girl4.png" )

player1 = Bitmap.new( texture_player )
player2 = Bitmap.new( texture_player )

player1:setAlpha( 0.5 )

player2:setScale( 1.25, 1.25 )
player2:setRotation( 45 )

stage:addChild( player1 )
stage:addChild( player2 )

player1:setPosition( 50, 150 )
player2:setPosition( 150, 150 )