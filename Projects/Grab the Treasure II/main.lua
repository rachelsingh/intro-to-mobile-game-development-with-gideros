-- Load our images
treasure_texture = Texture.new( "content/treasure.png" )

background = Bitmap.new( Texture.new( "content/background.png" ) )
treasure = Bitmap.new( treasure_texture )
player = Bitmap.new( Texture.new( "content/player.png" ) )

-- Load sounds
pickupSound = Sound.new( "content/Pickup_Coin3.wav" )
music = Sound.new( "content/DancingBunnies_Moosader.mp3" )

-- Load text
font = TTFont.new( "content/Amburegul.ttf", 20 )
scoreText = TextField.new( font, "Score: 0" )
scoreText:setPosition( 10, 470 )
scoreText:setTextColor( 0x000000 )

-- Draw images
stage:addChild( background )
stage:addChild( player )
stage:addChild( treasure )

-- Draw text
stage:addChild( scoreText )

-- Play music
musicChannel = music:play();
musicChannel:setLooping( true )
musicChannel:setPaused( true )

score = 0
moveTreasureCounter = 100
treasureX = 0
treasureY = 0

playerX = 320/2 - 16
playerY = 480/2 - 16
goalX = playerX
goalY = playerY
moveSpeed = 1
player:setPosition( playerX, playerY )

-- Functions
function RandomlyPlaceTreasure()
	treasureX = math.random( 0, 320 - 64 )
	treasureY = math.random( 0, 480 - 64 )
	
	print( "Treasure location: ", treasureX, treasureY )
	treasure:setPosition( treasureX, treasureY )
	
	-- Countdown Timer
	moveTreasureCounter = math.random( 1000, 2000 )
end

function HandleClick( event )
	goalX = event.x
	goalY = event.y
end

function Distance( x1, y1, x2, y2 )
	xd = x2 - x1
	yd = y2 - y1
	return math.sqrt( xd*xd + yd*yd )
end

function Update( event )	
	moveTreasureCounter = moveTreasureCounter - 1
	
	if ( moveTreasureCounter == 0 ) then
		print( "Timer is 0, move the treasure" )
		RandomlyPlaceTreasure()
	end
	
	if ( goalX < playerX ) then
		playerX = playerX - moveSpeed
	elseif ( goalX > playerX ) then
		playerX = playerX + moveSpeed
	end
	
	if ( goalY < playerY ) then
		playerY = playerY - moveSpeed
	elseif ( goalY > playerY ) then
		playerY = playerY + moveSpeed
	end
	
	player:setPosition( playerX, playerY )
	
	distance = Distance( playerX, playerY, treasureX, treasureY )
	if ( Distance( playerX + 16, playerY + 16, treasureX + 16, treasureY + 16 ) < 20 ) then
		score = score + 1
		print( "Grab treasure, score = ", score )
		RandomlyPlaceTreasure()
		pickupSound:play()
		scoreText:setText( "Score: " .. score )
	end
end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )
stage:addEventListener( Event.ENTER_FRAME, Update, self )

-- Game Start
RandomlyPlaceTreasure()