music = Sound.new( "content/SuburbanDinosaur_Moosader.mp3" )
effect = Sound.new( "content/Jump11.wav" )

channel = nil

buttonMusic = Bitmap.new( Texture.new( "content/MusicButton.png" ) )
buttonSound = Bitmap.new( Texture.new( "content/SoundButton.png" ) )

buttonMusic:setPosition( 60, 60 )
buttonSound:setPosition( 60, 160 )

stage:addChild( buttonMusic )
stage:addChild( buttonSound )

function HandleClick( event )
	if ( buttonMusic:hitTestPoint( event.x, event.y ) ) then
		
		if ( channel == nil ) then
			print( "Begin music" )
			channel = music:play()
			channel:setPitch( 1.5 )
			
		elseif ( channel:isPaused() ) then
			print( "Unpause" )
			channel:setPaused( false )
			
		else
			print( "Pause" )
			channel:setPaused( true )
			
		end
		
	elseif ( buttonSound:hitTestPoint( event.x, event.y ) ) then
		print( "Play sound" )
		effect:play()
		
	end
end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )
