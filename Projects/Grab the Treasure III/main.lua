-- Load our images
treasure_texture = Texture.new( "content/treasure.png" )
zombie_texture = Texture.new( "content/enemy.png" )

background = Bitmap.new( Texture.new( "content/background.png" ) )
treasure = Bitmap.new( treasure_texture )
player = Bitmap.new( Texture.new( "content/player.png" ) )
enemies = {}
-- Add enemies
zombieCount = 2
for i = 1, zombieCount do
	enemies[i] = Bitmap.new( zombie_texture )
	local x = math.random( 0, 320 - 64 )
	local y = math.random( 0, 480 - 64 )
	enemies[i]:setPosition( x, y )
	enemies[i]:setScale( 0.7, 0.7 )
end

-- Load sounds
pickupSound = Sound.new( "content/Pickup_Coin3.wav" )
music = Sound.new( "content/DancingBunnies_Moosader.mp3" )

-- Load text
font = TTFont.new( "content/Amburegul.ttf", 20 )
bigFont = TTFont.new( "content/Amburegul.ttf", 40 )
scoreText = TextField.new( font, "Score: 0" )
scoreText:setPosition( 200, 460 )
scoreText:setTextColor( 0xFFFFFF )
gameoverText = TextField.new( bigFont, "Game Over!" )
gameoverText:setTextColor( 0xFF0000 )
gameoverText:setPosition( 40, 480/2 )

-- Draw images
stage:addChild( background )
stage:addChild( player )
for i = 1, zombieCount do
	stage:addChild( enemies[i] )
end
stage:addChild( treasure )

-- Draw text
stage:addChild( scoreText )

-- Play music
musicChannel = music:play();
musicChannel:setLooping( true )

score = 0
enemiespeed = 0.25
moveTreasureCounter = 100
treasureX = 0
treasureY = 0

playerAlive = true
playerX = 320/2 - 16
playerY = 480/2 - 16
goalX = playerX
goalY = playerY
moveSpeed = 1
player:setPosition( playerX, playerY )

-- Functions
function RandomlyPlaceTreasure()
	treasureX = math.random( 0, 320 - 64 )
	treasureY = math.random( 0, 480 - 64 )
	
	print( "Treasure location: ", treasureX, treasureY )
	treasure:setPosition( treasureX, treasureY )
	
	-- Countdown Timer
	moveTreasureCounter = math.random( 1000, 2000 )
end

function HandleClick( event )
	if ( playerAlive ) then
		goalX = event.x
		goalY = event.y
	end
end

function Distance( x1, y1, x2, y2 )
	xd = x2 - x1
	yd = y2 - y1
	return math.sqrt( xd*xd + yd*yd )
end

function Update( event )	
	-- Timer to respawn the treasure
	moveTreasureCounter = moveTreasureCounter - 1
	
	if ( moveTreasureCounter == 0 ) then
		print( "Timer is 0, move the treasure" )
		RandomlyPlaceTreasure()
	end
	
	-- Player movement
	if ( goalX < playerX ) then 			playerX = playerX - moveSpeed
	elseif ( goalX > playerX ) then 	playerX = playerX + moveSpeed
	end
	
	if ( goalY < playerY ) then 			playerY = playerY - moveSpeed
	elseif ( goalY > playerY ) then 	playerY = playerY + moveSpeed
	end
	
	player:setPosition( playerX, playerY )
	
	-- Enemy movement
	for i = 1, zombieCount do
		zombieX, zombieY = enemies[i]:getPosition()
		
		if ( playerX < zombieX ) then			zombieX = zombieX - enemiespeed
		elseif ( playerX > zombieX ) then 	zombieX = zombieX + enemiespeed
		end
		
		if ( playerY < zombieY ) then			zombieY = zombieY - enemiespeed
		elseif ( playerY > zombieY ) then 	zombieY = zombieY + enemiespeed
		end
		
		enemies[i]:setPosition( zombieX, zombieY )
		
		-- Zombie / Player collision?
		if ( Distance( playerX + 16, playerY + 16, zombieX + 16, zombieY + 16 ) < 20 ) then
			stage:addChild( gameoverText )
			playerAlive = false
			player:setRotation( -90 )
		end
	end
	
	-- Check collisions
	if ( Distance( playerX + 16, playerY + 16, treasureX + 16, treasureY + 16 ) < 20 ) then
		score = score + 1
		print( "Grab treasure, score = ", score )
		RandomlyPlaceTreasure()
		pickupSound:play()
		scoreText:setText( "Score: " .. score )
	end
end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )
stage:addEventListener( Event.ENTER_FRAME, Update, self )

-- Game Start
RandomlyPlaceTreasure()