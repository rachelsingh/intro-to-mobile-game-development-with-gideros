-- Keycodes: http://docs.giderosmobile.com/reference/gideros/KeyCode#KeyCode

object = Bitmap.new( Texture.new( "treasure.png" ) )
object:setAnchorPoint( 0.5, 0.5 )
object:setPosition( 320/2, 480/2 )
stage:addChild( object )

xVel = 0
yVel = 0
speed = 2

function KeyDown( event )
	if ( event.keyCode == KeyCode.W or event.keyCode == KeyCode.UP ) then
		yVel = -speed
	elseif ( event.keyCode == KeyCode.S or event.keyCode == KeyCode.DOWN ) then
		yVel = speed
	elseif ( event.keyCode == KeyCode.A or event.keyCode == KeyCode.LEFT ) then
		xVel = -speed
	elseif ( event.keyCode == KeyCode.D or event.keyCode == KeyCode.RIGHT ) then
		xVel = speed
	end
end

function KeyUp( event )
	if ( event.keyCode == KeyCode.W or event.keyCode == KeyCode.S
			 or event.keyCode == KeyCode.UP or event.keyCode == KeyCode.DOWN ) then
		yVel = 0	
	elseif ( event.keyCode == KeyCode.A or event.keyCode == KeyCode.D
			 or event.keyCode == KeyCode.LEFT or event.keyCode == KeyCode.RIGHT)  then
		xVel = 0	
	end
end

function Update( event )
	local x, y = object:getPosition()
	
	x = x + xVel
	y = y + yVel

	object:setPosition( x, y )
end

stage:addEventListener( Event.KEY_DOWN, KeyDown, self )
stage:addEventListener( Event.KEY_UP, KeyUp, self )
stage:addEventListener( Event.ENTER_FRAME, Update, self )