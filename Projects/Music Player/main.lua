local channel = nil
local songLength = 0
musicA = Sound.new( "music/TunaFish_Moosader.mp3" )
musicB = Sound.new( "music/DancingBunnies_Moosader.mp3" )
musicC = Sound.new( "music/ClearDay_Moosader.mp3" )

btnPlay = Bitmap.new( Texture.new( "images/btnPlay.png" ) )
btnPlay:setPosition( 25, 50 )
stage:addChild( btnPlay )

btnPause = Bitmap.new( Texture.new( "images/btnPause.png" ) )
btnPause:setPosition( 125, 50 )
stage:addChild( btnPause )

btnStop = Bitmap.new( Texture.new( "images/btnStop.png" ) )
btnStop:setPosition( 225, 50 )
stage:addChild( btnStop )

btnPitchUp = Bitmap.new( Texture.new( "images/btnPitchUp.png" ) )
btnPitchUp:setPosition( 25, 150 )
stage:addChild( btnPitchUp )

btnLoud = Bitmap.new( Texture.new( "images/btnLoud.png" ) )
btnLoud:setPosition( 125, 150 )
stage:addChild( btnLoud )

btnHalfWay = Bitmap.new( Texture.new( "images/btnHalfWay.png" ) )
btnHalfWay:setPosition( 225, 150 )
stage:addChild(btnHalfWay )

btnPitchDown = Bitmap.new( Texture.new( "images/btnPitchDown.png" ) )
btnPitchDown:setPosition( 25, 250 )
stage:addChild( btnPitchDown )

btnQuiet = Bitmap.new( Texture.new( "images/btnQuiet.png" ) )
btnQuiet:setPosition( 125, 250 )
stage:addChild( btnQuiet )

btnBeginning = Bitmap.new( Texture.new( "images/btnBeginning.png" ) )
btnBeginning:setPosition( 225, 250 )
stage:addChild(btnBeginning )

btnSongA = Bitmap.new( Texture.new( "images/btnSongA.png" ) )
btnSongA:setPosition( 25, 350 )
stage:addChild( btnSongA )

btnSongB = Bitmap.new( Texture.new( "images/btnSongB.png" ) )
btnSongB:setPosition( 125, 350 )
stage:addChild( btnSongB )

btnSongC = Bitmap.new( Texture.new( "images/btnSongC.png" ) )
btnSongC:setPosition( 225, 350 )
stage:addChild(btnSongC )

function HandleClick( event )
	if ( btnSongA:hitTestPoint( event.x, event.y ) ) then
		if ( channel ~= nil ) then	channel:stop()		end
		channel = musicA:play()
		songLength = musicA:getLength()
	
	elseif ( btnSongB:hitTestPoint( event.x, event.y ) ) then
		if ( channel ~= nil ) then	channel:stop()		end
		channel = musicB:play()
		songLength = musicB:getLength()
	
	elseif ( btnSongC:hitTestPoint( event.x, event.y ) ) then
		if ( channel ~= nil ) then	channel:stop()		end
		channel = musicC:play()
		songLength = musicC:getLength()
	
	-- POSITION
	elseif ( btnBeginning:hitTestPoint( event.x, event.y ) ) then
		channel:setPosition( 0 )
		
	elseif ( btnHalfWay:hitTestPoint( event.x, event.y ) ) then
		channel:setPosition( songLength / 2 )
		
	-- PITCH
	elseif ( btnPitchUp:hitTestPoint( event.x, event.y ) ) then
		channel:setPitch( 1.5 )
		
	elseif ( btnPitchDown:hitTestPoint( event.x, event.y ) ) then
		channel:setPitch( 0.5 )
		
	-- VOLUME
	elseif ( btnQuiet:hitTestPoint( event.x, event.y ) ) then
		channel:setVolume( 0.5 )
		
	elseif ( btnLoud:hitTestPoint( event.x, event.y ) ) then
		channel:setVolume( 1.0 )
		
	-- CONTROL
	elseif ( btnPause:hitTestPoint( event.x, event.y ) ) then
		channel:setPaused( true )
		
	elseif ( btnPlay:hitTestPoint( event.x, event.y ) ) then
		channel:setPause( false )
		
	elseif ( btnStop:hitTestPoint( event.x, event.y ) ) then
		channel:stop()
	
	end

end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )