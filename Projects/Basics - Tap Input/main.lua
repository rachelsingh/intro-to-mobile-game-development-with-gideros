tapCount = 0

player = Bitmap.new( Texture.new( "content/girl4.png" ) )
player:setPosition( 320/2, 480/2 )
stage:addChild( player )

font = TTFont.new( "content/Amburegul.ttf", 15 )
text = TextField.new( font, "Taps: 0" )
text:setPosition( 20, 25 )
stage:addChild( text )

function HandleClick( event )
	player:setPosition( event.x, event.y )
	
	tapCount = tapCount + 1
	text:setText( "Taps: " .. tapCount )
end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )