-- Load our images
treasure_texture = Texture.new( "content/treasure.png" )

background = Bitmap.new( Texture.new( "content/background.png" ) )
treasure = Bitmap.new( treasure_texture )

-- Load sounds
pickupSound = Sound.new( "content/Pickup_Coin3.wav" )
music = Sound.new( "content/DancingBunnies_Moosader.mp3" )

-- Load text
font = TTFont.new( "content/Amburegul.ttf", 20 )
scoreText = TextField.new( font, "Score: 0" )
scoreText:setPosition( 40, 30 )
scoreText:setTextColor( 0x8d8d8d )

-- Draw images
stage:addChild( background )
stage:addChild( treasure )

-- Draw text
stage:addChild( scoreText )

-- Play music
musicChannel = music:play()
musicChannel:setLooping( true )

score = 0
moveTreasureCounter = 100

-- Functions
function RandomlyPlaceTreasure()
	local randomX = math.random( 0, 320 - 64 )
	local randomY = math.random( 0, 480 - 64 )
	
	print( "Treasure location: ", randomX, randomY )
	treasure:setPosition( randomX, randomY )
	
	-- Countdown Timer
	moveTreasureCounter = math.random( 50, 200 )
end

function HandleClick( event )
	if ( treasure:hitTestPoint( event.x, event.y ) ) then
		score = score + 1
		print( "Grab treasure, score = ", score )
		RandomlyPlaceTreasure()
		pickupSound:play()
		scoreText:setText( "Score: " .. score )
	end
end

function Update( event )	
	moveTreasureCounter = moveTreasureCounter - 1
	
	if ( moveTreasureCounter == 0 ) then
		print( "Timer is 0, move the treasure" )
		RandomlyPlaceTreasure()
	end
end

stage:addEventListener( Event.MOUSE_DOWN, HandleClick, self )
stage:addEventListener( Event.ENTER_FRAME, Update, self )

-- Game Start
RandomlyPlaceTreasure()